//
//  Grayscale.swift
//  EmotionDetection
//
//  Created by Giovanni Di Guida on 06/04/2020.
//  Copyright © 2020 Giovanni Di Guida. All rights reserved.
//

import UIKit

extension UIImage{
    var mono: UIImage {
        let context = CIContext(options: nil)
        let currentFilter = CIFilter(name: "CIPhotoEffectMono")!
        currentFilter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        let output = currentFilter.outputImage!
        let cgImage = context.createCGImage(output, from: output.extent)!
        let processedImage = UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
        return processedImage
    }
}

//We will use the grayscale because the MLModel is trained with grayscale images
