//
//  ViewController.swift
//  Workshop - Start
//
//  Created by Giovanni Di Guida on 06/04/2020.
//  Copyright © 2020 NoSynapses. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var predictionLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.isUserInteractionEnabled = true
        let tapGesture=UITapGestureRecognizer(target: self, action: #selector(didTapOnImage(tapGesture:)))
        imageView.addGestureRecognizer(tapGesture)
    }

    @objc func didTapOnImage(tapGesture: UITapGestureRecognizer){
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }

}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else{
            dismiss(animated: true, completion: nil)
            return
        }
        dismiss(animated: true, completion: nil)
        
        imageView.image = image
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func classifyImage(for image: UIImage){
        
    }
}

